CREATE DATABASE manage_product;
USE manage_product;

CREATE TABLE brands(
    id INTEGER AUTO_INCREMENT NOT NULL,
    name VARCHAR(50) NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO brands VALUES(1,'New brand', now(), now());
INSERT INTO brands VALUES(2,'Old brand', now(), now());

CREATE TABLE products(
    id BIGINT AUTO_INCREMENT NOT NULL,
    sku VARCHAR(12) UNIQUE NOT NULL,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    currency VARCHAR(12) NOT NULL,
    id_brand INTEGER NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    PRIMARY KEY(id),
    INDEX (sku),
    FOREIGN KEY(id_brand) REFERENCES brands(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO products VALUES(1,'SK987654', 'Product one', 250.01, 'pesos', 1, now(), now(), TRUE);
INSERT INTO products VALUES(2,'SK987653', 'Product two', 520.30, 'pesos', 2, now(), now(), TRUE);


CREATE TABLE user_type(
    id INTEGER AUTO_INCREMENT NOT NULL,
    label VARCHAR(25) NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    active BOOLEAN NOT NULL,
   PRIMARY KEY(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO user_type VALUES(1,'admin', now(), now(), TRUE);
INSERT INTO user_type VALUES(2,'anonymous', now(), now(), TRUE);


CREATE TABLE users(
    id INTEGER AUTO_INCREMENT NOT NULL,
    username varchar(50) UNIQUE NOT NULL,
    password varchar(256) NOT NULL,
    id_user_type integer NOT NULL,
    email varchar(50) NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	active BOOLEAN NOT NULL,

    PRIMARY KEY(id),
    FOREIGN KEY(id_user_type) REFERENCES user_type(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO users VALUES(1,'admin1','QWE1',1,'test1@gmail.com', now(), now(), TRUE);
INSERT INTO users VALUES(2,'admin2','QWE2',1,'test1@gmail.com', now(), now(), TRUE);
INSERT INTO users VALUES(3,'admin3','QWE3',1,'test1@gmail.com', now(), now(), TRUE);


CREATE TABLE requests(
    id BIGINT AUTO_INCREMENT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_sku BIGINT NOT NULL,
    id_user_type INTEGER NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(id_sku) REFERENCES products(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


