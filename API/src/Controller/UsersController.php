<?php

namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\View\Exception\MissingTemplateException;


class UsersController extends AppController{
  private $_idAdmin = 1;
  private $_idAnonymous = 2;
  
  public function createAdmin($DataNewAdmin){
    $this->loadModel('Users');
    $dataUser = $this->Users->newEntity([
        'username'   => $DataNewAdmin['username'],
        'password'  => $DataNewAdmin['password'],
        'email'  => $DataNewAdmin['email'],
        'id_user_type'  => $this->_idAdmin,
        'active' => TRUE,
      ]);
      $this->Users->save($dataUser);
  }
  
  public function deleteAdmin($username){
    $this->loadModel('Users');
    $user = $this->Users->find()->select([
      'id' => 'id',
    ])
    ->Where(['username'=>$username]);
    
      $aUsers = $user->toArray();
      $oUsers = $this->Users->get($aUsers[0]['id']);
      $oUsers->active = FALSE;
      $this->Users->save($oUsers);
  }
  
  public function updateAdmin($dataUser){
    $this->loadModel('Users');
    $username = $dataUser['username'];
    $user = $this->Users->find()->select([
      'id' => 'id',
    ])
    ->Where(['username'=>$username]);
      $aUser = $user->toArray();
      $oUser = $this->Users->get($aUser[0]['id']);
      $modifiedUser = $this->Users->patchEntity($oUser, $dataUser);
      $this->Users->save($modifiedUser);
      
  }
  
  public function getAllAdminEmails(){
    $this->loadModel('Users');
    $adminEmailsResultset = $this->Users->find()->select([
      'email' => 'email'
    ])
    ->Where(['active'=>TRUE]);
    $emails = $adminEmailsResultset->toArray();
    return $emails;
  }
}
