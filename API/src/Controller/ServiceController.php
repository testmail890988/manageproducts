<?php

namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\UsersController;
use App\Controller\ProductsController;
use App\Controller\RequestController;
use App\Controller\EmailController;

class ServiceController extends AppController{
  private $_failMessage = ['Result' => 'Fail'];
  private $_successMessage = ['Result' => 'Success'];
  private $_idAdmin = 1;
  private $_idAnonymous = 2;
  private function _hasUserData($json){
    $hasUserData = FALSE;
    if(isset($json['user']) && isset($json['user']['username'])&&isset($json['user']['password'])){
      $hasUserData = TRUE;
    }
    return $hasUserData;
  }
  
  private function _isAdmin($json)
  { 
    $isAdmin = FALSE;
    $hasUserData=$this->_hasUserData($json);
    if($hasUserData){
    $aUser = $json['user'];
    $username = $aUser['username'];
    $password = $aUser['password'];
    $this->loadModel('Users');
    $resultset = $this->Users->find()
            ->innerJoin(['UserType' => 'user_type'], ['Users.id_user_type = UserType.id'])
            ->Where(['username'=> $username, 'password'=>$password ,'Users.active'=>TRUE, 
                'UserType.active'=>TRUE, 'UserType.id' => 1]);
    $resultset = $resultset->toArray();
    if(!empty($resultset) && isset($resultset[0])&& isset($resultset[0]['id'])){
      $isAdmin = TRUE;
    }
    }
    return $isAdmin;
  }
  
  public function readProducts()
  {
    $this->disableAutoRender();
    $response = $this->response;
    $ProductsController = new ProductsController();
    $products = $ProductsController->getAllProducts();
    $response = $response->withType('application/json')->withStringBody(json_encode(['products' => $products]));
    return$response;
  }
  public function readProduct()
  {
    $this->disableAutoRender();
    $json = $this->request->getData();
    $response = $this->response;
    $message = $this->_failMessage; 
    $isAdmin = $this->_isAdmin($json);
    $idUserType = $this->_idAnonymous;
    if($isAdmin){
      $idUserType = $this->_idAdmin;
    }
    $aProduct = $json['data'];
    $sku = $aProduct['sku'];
    $ProductsController = new ProductsController();
    $product = $ProductsController->getProduct($sku);
    $idSku = $product[0]['id'];
    unset($product['id']);
    $RequestController = new RequestController();
    $RequestController->addRequest($idSku,$idUserType);
    $message = ['product' => $product];
    $response = $response->withType('application/json')->withStringBody(json_encode($message));
    return$response;
  }
  public function createProduct()
  {
    $this->disableAutoRender();
    $json = $this->request->getData();
    $message = $this->_failMessage;
    if($this->_isAdmin($json)){
      $arrayNewProduct = $json['data'];
      $ProductsController = new ProductsController();
      $ProductsController->createProduct($arrayNewProduct);
      $message = $this->_successMessage;
    }
    $response = $this->response;
    $response = $response->withType('application/json')->withStringBody(json_encode($message));
    return $response;
  }
  
   public function deleteProduct()
  {
    $this->disableAutoRender();
    $json = $this->request->getData();
    $isAdmin = $this->_isAdmin($json);
    $message = $this->_failMessage;
    if($isAdmin){
      $arrayProduct = $json['data'];
      $sku = $arrayProduct['sku'];
      $ProductsController = new ProductsController();
      $ProductsController->deleteProduct($sku);
      $response = $this->response;
      $message = $this->_successMessage;
    }
    $response = $response->withType('application/json')->withStringBody(json_encode($message));    
    return $response;
  }
  
  public function updateProduct()
  {
    $this->disableAutoRender();
    
    $json = $this->request->getData();
    $response = $this->response;
    $message = $this->_failMessage; 
    $isAdmin = $this->_isAdmin($json);
    if($isAdmin){
    $arrayProduct = $json['data'];
    $ProductsController = new ProductsController();
    $ProductsController->updateProduct($arrayProduct);
    $UsersController = new UsersController();
    $emailAdminEmails = $UsersController->getAllAdminEmails();
    $sku = $arrayProduct['sku'];
    $EmailController = new EmailController();
    $EmailController->sendAlert($emailAdminEmails,$sku);
    $message = $this->_successMessage; 
    }

    $response = $response->withType('application/json')->withStringBody(json_encode($message));    
    return $response;
  }
   

  
  public function createAdmin()
  {
    $this->disableAutoRender();
    $json = $this->request->getData();
    $isAdmin = $this->_isAdmin($json);
    $message = $this->_failMessage;
    if($isAdmin){
    $arrayNewUser = $json['data'];
    $UsersController = new UsersController();
    $UsersController->createAdmin($arrayNewUser);
        $message = $this->_successMessage;
    }
    $response = $this->response;
    $response = $response->withType('application/json')->withStringBody(json_encode($message));
    return $response;
  }
  
   public function deleteAdmin()
  {
    $this->disableAutoRender();
    $json = $this->request->getData();
    $isAdmin = $this->_isAdmin($json);
    $message = $this->_failMessage;
    if($isAdmin){ 
    $arrayDeleteUser = $json['data'];
    $username = $arrayDeleteUser['username'];
    $UsersController = new UsersController();
    $UsersController->deleteAdmin($username);
    $message = $this->_successMessage;
    }
    $response = $this->response;
    $response = $response->withType('application/json')->withStringBody(json_encode($message));
    return $response;
  }
  
  public function updateAdmin()
  {
    $this->disableAutoRender();
    
    $json = $this->request->getData();
    $isAdmin = $this->_isAdmin($json);
    $message = $this->_failMessage;
    if($isAdmin){ 
    $arrayEditUser = $json['data'];
    $UsersController = new UsersController();
    $UsersController->updateAdmin($arrayEditUser);
    $message = $this->_successMessage;
    }
    $response = $this->response;
    $response = $response->withType('application/json')->withStringBody(json_encode($message));
    return $response;
  }  
}
