<?php

namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\UsersController;

class ProductsController extends AppController{

  public function getAllProducts(){
    $this->loadModel('Products');
    $productsResultset = $this->Products->find()->select([
      'sku' => 'sku',
      'name' => 'Products.name',
      'price' => 'price',
      'currency' => 'currency',
      'brand' => 'Brands.name'
    ])
    ->innerJoin(['Brands' => 'brands'], ['Products.id_brand = Brands.id']);
    $products = $productsResultset->toArray();
    return $products;
  }
  
  public function getProduct($sku){
    $this->loadModel('Products');
    $productResultset = $this->Products->find()->select([
      'id' => 'Products.id',
      'sku' => 'sku',
      'name' => 'Products.name',
      'price' => 'price',
      'currency' => 'currency',
      'brand' => 'Brands.name'
    ])
    ->innerJoin(['Brands' => 'brands'], ['Products.id_brand = Brands.id'])
    ->Where(['sku'=>$sku]);
    $product = $productResultset->toArray();
    return $product;
  }
  
  private function _getBrandByName($name){
     $this->loadModel('Brands');
    $brandResultset = $this->Brands->find()->select([
      'id' => 'id',
      'name' => 'name'
    ])
    ->Where(['name'=>$name]);
    $dataBrand = $brandResultset->toArray();
    $dataBrandResult = empty($dataBrand)?null:$dataBrand;
    return $dataBrandResult;
  }
  
    private function _createNewBrand($name){
     $this->loadModel('Brands');
      $brand = $this->Brands->newEntity([
        'name'  => $name,
      ]);
    $brand =  $this->Brands->save($brand);
    return $brand->id;
  }
  
  public function createProduct($DataNewProduct){
    date_default_timezone_set('America/Mexico_City');
    $now = Time::now()->i18nFormat('yyyy-MM-dd HH:mm:ss');
    $dataBrand = $this->_getBrandByName($DataNewProduct['brand']);
    if(!$dataBrand){
      $idBrand = $this->_createNewBrand($DataNewProduct['brand']); 
    } else {
      $idBrand = $dataBrand[0]['id'];
    }
    $this->loadModel('Products');
    $dataProduct = $this->Products->newEntity([
        'sku'   => $DataNewProduct['sku'],
        'name'  => $DataNewProduct['name'],
        'price' => $DataNewProduct['price'],
        'currency' => $DataNewProduct['currency'],
        'id_brand' => $idBrand,
      ]);
      $this->Products->save($dataProduct);
  }
  
  public function deleteProduct($sku){
    $this->loadModel('Products');
    $product = $this->Products->find()->select([
      'id' => 'id',
    ])
    ->Where(['sku'=>$sku]);
      $aProduct = $product->toArray();
      $oProduct = $this->Products->get($aProduct[0]['id']);
      $oProduct->active = FALSE;
      $this->Products->save($oProduct);
  }
  
  public function updateProduct($dataProduct){
    $this->loadModel('Products');
    $sku = $dataProduct['sku'];
    $product = $this->Products->find()->select([
      'id' => 'id',
    ])
    ->Where(['sku'=>$sku]);
      $aProduct = $product->toArray();
      $oProduct = $this->Products->get($aProduct[0]['id']);
      $modifiedProduct = $this->Products->patchEntity($oProduct, $dataProduct);
      $this->Products->save($modifiedProduct);
  }
  
  public function _addRequest($idSku, $idTypeUser){
    $this->loadModel('Requests');
    $dataRequest = $this->Requests->newEntity([
        'id_sku'   => $idSku,
        'id_user_type'  => $idTypeUser,
      ]);
      $this->Requests->save($dataRequest);
  }
}
