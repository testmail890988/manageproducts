<?php

namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\View\Exception\MissingTemplateException;
require_once ROOT . DS . 'vendor' . DS . 'phpmailer' . DS . 'phpmailer'. DS . 'src' . DS . 'Exception.php';
require_once ROOT . DS . 'vendor' . DS . 'phpmailer' . DS . 'phpmailer'  . DS . 'src'. DS . 'PHPMailer.php';
require_once ROOT . DS . 'vendor' . DS . 'phpmailer' . DS . 'phpmailer'. DS . 'src' . DS . 'SMTP.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class EmailController extends AppController{

    public function sendAlert($mails, $sku){
      $title = 'Product ' .$sku. ' has been modified.';
      $body = 'Product ' .$sku. ' has been modified.';
      $this->_sendMail($mails, $title, $body);
    }
    private function _sendMail($mails,$title, $body, $altBody = '', $isHTML = true, $isUTF8 = false){
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = \Cake\Core\Configure::read('SmtpConfig')['Host']; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = \Cake\Core\Configure::read('SmtpConfig')['Username']; // SMTP username
        $mail->Password = \Cake\Core\Configure::read('SmtpConfig')['Password'];  // SMTP password
        //$mail->SMTPSecure = 'ssl';                          // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;      //Enable implicit TLS encryption
        $mail->Port = 465;                                    // TCP port to connect to
        if($isUTF8){
          $mail->CharSet = 'UTF-8';
          $mail->Encoding = 'base64';
        }
        //Recipients
        $mail->setFrom(\Cake\Core\Configure::read('SmtpConfig')['Username']);

        // Add a recipient
        if(!empty($mails)){
          foreach($mails as $email){
              $mail->addAddress($email['email']);
              }
        }else{
          return array('status' => 'fail', 'message' => 'No email found');
        }
        
        //Content
        $mail->isHTML($isHTML);                                  // Set email format to HTML
        $mail->Subject = (!empty($title) ? $title : '');
        $mail->Body    = (!empty($body) ? $body : '');
        $mail->AltBody = (!empty($altBody) ? $altBody : '');

        $mail->send();
        return array('status' => 'ok', 'message' => 'ok');
    } catch (\Exception $e) {
      return array('status' => 'fail', 'message' =>  $mail->ErrorInfo);
    }
  }
}