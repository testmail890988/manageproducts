<?php

namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\View\Exception\MissingTemplateException;

class RequestController extends AppController{

  public function addRequest($idSku, $idTypeUser){
    $this->loadModel('Requests');
    $dataRequest = $this->Requests->newEntity([
        'id_sku'   => $idSku,
        'id_user_type'  => $idTypeUser,
      ]);
      $this->Requests->save($dataRequest);
  }
}
