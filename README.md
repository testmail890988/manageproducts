# ManageProducts

# Installation

* Install Apache2
* Install PHP 7.4
* Install CakePhp https://book.cakephp.org/4/en/installation.html
* Create database (Mysql), Script file in project Scripts/20211017.sql
* Create tmp and logs folders.
* Config your DB connection in config/app_local.php
* Config SMTP credentials in config/app_local.php
```
    'SmtpConfig' => [
      'Host' => '',
      'Username'  => '',
      'Password' => '',
    ],
```
# API
## Read a Product

**URL:** http://{{host}}/ManageProducts/API/Service/readProduct

**Example as Admin:**
```
{ 
	"user":{
		"username":"admin1",
		"password":"QWE1"
	},"data" :{    
       "sku" : "SK987653"
	}
}
```
**Example as Anonymous:**
```
{ 
"data" :{    
       "sku" : "SK987653"
	}
}
```
**Result:**
```
{
    "product": [
        {
            "id": 2,
            "sku": "SK987653",
            "name": "Product two",
            "price": "520.30",
            "currency": "pesos",
            "brand": "Old brand"
        }
    ]
}
```
## Create a Product
**URL:** http://{{host}}/ManageProducts/API/Service/createProduct

**Example:**
```
{
	"user":{
		"username":"admin1",
		"password":"QWE1"
	},
	"data" :{    
       "sku" : "SK90019",
      "name" : "newsss p",
      "price": 2020.56,
      "currency" : "pesos",
      "brand" : "Brand3"
	}
}
```
**Result:**
```
{
    "Result": "Success"
}
```
## Update a Product
**URL:** http://{{host}}/ManageProducts/API/Service/updateProduct


**Example:**
```
{ 
	"user":{
		"username":"admin1",
		"password":"QWE1"
	},
	"data" :{    
       "sku" : "SK987654",
      "name" : "new p",
      "price": 20.56,
      "currency" : "pesos",
      "brand" : "new brand 2"
	}
}
```
**Result:**
```
{
    "Result": "Success"
}
```

## Delete a Product
**URL:** http://{{host}}/ManageProducts/API/Service/deleteProduct


**Example:**
```
{ 
		"user":{
		"username":"admin1",
		"password":"QWE1"
	},
	"data" :{    
       "sku" : "SK987654"
	}
}
```
**Result:**
```
{
    "Result": "Success"
}
```

## Create an Admin
**URL:** http://{{host}}/ManageProducts/API/Service/createAdmin

**Example:**
```
{ "user" :{    
       "username" : "admin1",
       "password" : "QWE1"
},"data":{
		"username":"admin15",
		"password":"QWE1",
		"email":"test10@gmail.com"
	}
}
```
**Result:**
```
{
    "Result": "Success"
}
```
## Update an Admin
**URL:** http://{{host}}/ManageProducts/API/Service/updateAdmin

**Example:**
```
{ 	"user":{
		"username":"admin1",
		"password":"QWE1"
	},"data" :{    
       "username" : "admin2",
       "password" : "new holasx",
       "email" : "test5@gmail.com"
	}
}
```
**Result:**
```
{
    "Result": "Success"
}
```

## Delete an Admin
**URL:** http://{{host}}/ManageProducts/API/Service/deleteAdmin

**Example:**
```
{	"user":{
		"username":"admin1",
		"password":"QWE1"
	}, "data" :{    
       "username" : "admin3"
	}
}
```
**Result:**
```
{
    "Result": "Success"
}
```


